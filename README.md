# ph-dynamodb
***
This is a collection helper functions for working with AWS DynamoDB

CreateUpdateQuery takes a interface of type struct then reads the "json:" dTag and "dynDB:" dTag and constructs  
a DynamoDB update sting from that. The value of the dynDB tag must be prefixed with :, and a json tag is also required.  

### Example:
***
In the example we want to update a User, we set the ID, Email and Name, but we do not assign a value to Surname.
this will result in an update string that looks like this: `"set id=:i, email=:e, name=:n"`
```go
type User struct {
	ID      string `json:":i" dynDB:"id"`
	Email   string `json:":e" dynDB:"email"`
	Name    string `json:":n" dynDB:"name"`
	Surname string `json:":s" dynDB:"surname"`
}
u := user{ID: 1, Email: "example@example.com", Name: "Ola"}
updatestring, err CreateUpdateQuery(u)
if err != nil {
	return err
}
```