package ph_dynamodb

import (
	"fmt"
	"reflect"
	"strings"
)

const dynTag = "dynDB"
const jsonTag = "json"

func writeQueryString(b *strings.Builder, jsonTag, dynTag string, structLength, index int) {
	b.WriteString(dynTag)
	b.WriteString("=")
	b.WriteString(jsonTag)
	if structLength != index {
		b.WriteString(", ")
	}
}

func validateDynTag(s string) error {
	prefix := s[0:1]
	if prefix != ":" {
		return fmt.Errorf("json tag value should be prefix with :")
	}
	return nil
}

func cleanJsonTag(t string) string {
	if strings.Contains(t, "omitempty") {
		s := strings.Split(t, ",")
		return s[0]
	}
	return t
}

// CreateUpdateQuery takes a interface of type struct then reads the "json:" dTag and "dynDB:" dTag and constructs
// a DynamoDB update sting from that. The value of the dynDB tag must be prefixed with :, and a json tag is also required.
//
// Tag example: OrderUUID string `json:"order_uuid" dynDB:":ou"`
func CreateUpdateQuery(o interface{}) (string, error) {
	k := reflect.TypeOf(o).Kind()
	if k == reflect.Ptr {
		return "", fmt.Errorf("interface can not be of type pointer")
	}
	// TypeOf returns the reflection Type that represents the dynamic type of variable.
	// If variable is a nil interface value, TypeOf returns nil.
	t := reflect.TypeOf(o)
	values := reflect.ValueOf(o)

	var b strings.Builder
	b.WriteString("set ")
	sl := t.NumField()
	var err error
	for i := 0; i < sl; i++ {
		// Get the field, returns https://golang.org/pkg/reflect/#StructField
		field := t.Field(i)
		value := values.Field(i)
		switch value.Kind() {
		case reflect.String:
			dTag := field.Tag.Get(dynTag)
			jTag := cleanJsonTag(field.Tag.Get(jsonTag))
			err = validateDynTag(jTag)
			if err != nil {
				return "", err
			}
			v := value.String()
			if v != "" {
				writeQueryString(&b, jTag, dTag, sl, i)
			}
		case reflect.Int:
			dTag := field.Tag.Get(dynTag)
			jTag := cleanJsonTag(field.Tag.Get(jsonTag))
			err = validateDynTag(jTag)
			if err != nil {
				return "", err
			}
			v := value.Int()
			if v != 0 {
				writeQueryString(&b, jTag, dTag, sl, i)
			}
		case reflect.Int32:
			dTag := field.Tag.Get(dynTag)
			jTag := cleanJsonTag(field.Tag.Get(jsonTag))
			err = validateDynTag(jTag)
			if err != nil {
				return "", err
			}
			v := value.Int()
			if v != 0 {
				writeQueryString(&b, jTag, dTag, sl, i)
			}
		case reflect.Int64:
			dTag := field.Tag.Get(dynTag)
			jTag := cleanJsonTag(field.Tag.Get(jsonTag))
			err = validateDynTag(jTag)
			if err != nil {
				return "", err
			}
			v := value.Int()
			if v != 0 {
				writeQueryString(&b, jTag, dTag, sl, i)
			}
		case reflect.Float32:
			dTag := field.Tag.Get(dynTag)
			jTag := cleanJsonTag(field.Tag.Get(jsonTag))
			err = validateDynTag(jTag)
			if err != nil {
				return "", err
			}
			v := value.Float()
			if v != 0 {
				writeQueryString(&b, jTag, dTag, sl, i)
			}
		case reflect.Float64:
			dTag := field.Tag.Get(dynTag)
			jTag := cleanJsonTag(field.Tag.Get(jsonTag))
			err = validateDynTag(jTag)
			if err != nil {
				return "", err
			}
			v := value.Float()
			if v != 0 {
				writeQueryString(&b, jTag, dTag, sl, i)
			}
		case reflect.Bool:
			dTag := field.Tag.Get(dynTag)
			jTag := cleanJsonTag(field.Tag.Get(jsonTag))
			err = validateDynTag(jTag)
			if err != nil {
				return "", err
			}
			v := value.Bool()
			if !v {
				writeQueryString(&b, jTag, dTag, sl, i)
			}
		default:
		}
	}
	return strings.TrimSuffix(b.String(), ", "), nil
}
